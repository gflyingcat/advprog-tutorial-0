package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cucumber extends Food {
    Food food;

    public Cucumber(Food food) {
        this.food = food;
        description = "Cucumber";
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding " + description.toLowerCase();
    }

    @Override
    public double cost() {
        return 0.4 + food.cost();
    }
}
