package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.Company;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;


public class CompanyLoader {
    public static void main(String[] args) {
        Company company = new Company();

        company.addEmployee(new Ceo("Mr. What Zit Tooya", 500000.0));
        company.addEmployee(new Cto("Squidward", 500000.0));

        company.addEmployee(new BackendProgrammer("Patrick", 50000.0));
        company.addEmployee(new FrontendProgrammer("Mr. Crab", 50000.0));

        System.out.println("List of employees: " + company.getAllEmployees());

        System.out.println("Total salaries: " + company.getNetSalaries());
    }
}