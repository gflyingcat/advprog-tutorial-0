package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChiliSauce extends Food {
    Food food;

    public ChiliSauce(Food food) {
        this.food = food;
        description = "Chili Sauce";
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding " + description.toLowerCase();
    }

    @Override
    public double cost() {
        return 0.3 + food.cost();
    }
}
