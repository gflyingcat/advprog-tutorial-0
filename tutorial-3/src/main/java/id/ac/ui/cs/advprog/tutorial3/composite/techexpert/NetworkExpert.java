package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class NetworkExpert extends Employees {
    public NetworkExpert(String name, double salary) {
        this.name = name;
        this.salary = salary;
        role = "Network Expert";

        if (salary < 50000.0) {
            throw new IllegalArgumentException("Network Expert salary must not lower than 50000");
        }
    }
}