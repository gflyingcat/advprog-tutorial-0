package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    public Ceo(String name, double salary) {
        this.name = name;
        this.salary = salary;
        role = "CEO";

        if (salary < 200000.0) {
            throw new IllegalArgumentException("CEO salary must not lower than 200000");
        }
    }
}
