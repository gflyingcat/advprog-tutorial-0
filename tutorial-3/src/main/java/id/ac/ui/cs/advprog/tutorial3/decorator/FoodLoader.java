package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChickenMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChiliSauce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Tomato;

public class FoodLoader {
    public static void main(String[] args) {
        Food yummyFood = new Cheese(
            new ChiliSauce(
                new Tomato(
                    new ChickenMeat(
                        new CrustySandwich()
                    )
                )
            )
        );

        System.out.println("This yummy food costs " + yummyFood.cost() +
            " and it consists of " + yummyFood.getDescription());
    }
}