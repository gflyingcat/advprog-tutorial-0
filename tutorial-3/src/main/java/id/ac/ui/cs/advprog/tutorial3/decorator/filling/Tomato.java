package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Tomato extends Food {
    Food food;

    public Tomato(Food food) {
        this.food = food;
        description = "Tomato";
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding " + description.toLowerCase();
    }

    @Override
    public double cost() {
        return 0.5 + food.cost();
    }
}
