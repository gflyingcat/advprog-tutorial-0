import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ScoreGroupingTest {

    private Map<String, Integer> scores;

    @Before
    public void setUp() {
        scores = new HashMap<>();
        scores.put("A", 10);
        scores.put("B", 12);
        scores.put("C", 12);
    }

    @Test
    public void groupByScoresTest() {
        Map<Integer, List<String>> answer = ScoreGrouping.groupByScores(scores);
        assertTrue(answer.keySet().contains(10) && answer.keySet().contains(12));
        assertEquals(2, answer.get(12).size());
    }
}
