import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public int frequentRenterPoints() {
        int result = 0;
        for (Rental rental: rentals) {
            result += rental.getFrequentRenterPoint();
        }

        return result;
    }

    public double totalAmount() {
        double result = 0;
        for (Rental rental: rentals) {
            result += rental.getAmount();
        }

        return result;
    }

    public String statement() {
        String result = "Rental Record for " + getName() + "\n";

        // Show figures for this rental
        for (Rental rental: rentals) {
            result += "\t" + rental.getMovie().getTitle();
            result += "\t" + String.valueOf(rental.getAmount()) + "\n";
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(totalAmount()) + "\n";
        result += "You earned " + String.valueOf(frequentRenterPoints());
        result += " frequent renter points";

        return result;
    }

    public String htmlStatement() {
        String result = "<h1>Rental Record for " + getName() + "</h1>";

        // Show figures for this rental
        result += "<table><tr><th>title</th><th>amount</th></tr>";
        for (Rental rental: rentals) {
            result += "<tr>";
            result += "<td>" + rental.getMovie().getTitle() + "</td>";
            result += "<td>" + String.valueOf(rental.getAmount()) + "</td>";
            result += "</tr>";
        }
        result += "</table>";

        // Add footer lines
        result += "<h3>Amount owed is " + String.valueOf(totalAmount()) + "</h3>";
        result += "<h3>You earned " + String.valueOf(frequentRenterPoints());
        result += " frequent renter points</h3>";

        return result;
    }

}