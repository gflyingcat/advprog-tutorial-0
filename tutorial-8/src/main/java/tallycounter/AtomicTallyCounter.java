package tallycounter;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicTallyCounter extends TallyCounter {
    private AtomicInteger counter = new AtomicInteger(0);

    @Override
    public synchronized void increment() {
        counter.incrementAndGet();
    }

    @Override
    public synchronized void decrement() {
        counter.decrementAndGet();
    }

    @Override
    public synchronized int value() {
        return counter.get();
    }
}
