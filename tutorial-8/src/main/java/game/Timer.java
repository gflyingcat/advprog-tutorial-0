package game;

import java.util.concurrent.atomic.AtomicInteger;

public class Timer implements Runnable {
    private AtomicInteger currentScore;
    private int secondsElapsed = 0;

    public Timer() {
        currentScore = new AtomicInteger(100);
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(1000);
                secondsElapsed += 1;
                currentScore.getAndDecrement();
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    public int getTime() {
        return secondsElapsed;
    }

    public void givePoints(double percentage) {
        currentScore.updateAndGet(x -> (int)(x * percentage / 100));
    }

    public int getPoints() {
        return currentScore.get();
    }
}
