
package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DepokPizzaStoreTest {

    PizzaStore dpStore;

    @BeforeEach
    void setUp() {
        dpStore = new DepokPizzaStore();
    }

    @Test
    void createPizza() {
        Pizza pizza = dpStore.orderPizza("cheese");
        assertNotNull(pizza);

        pizza = dpStore.orderPizza("clam");
        assertNotNull(pizza);

        pizza = dpStore.orderPizza("veggie");
        assertNotNull(pizza);
    }

}